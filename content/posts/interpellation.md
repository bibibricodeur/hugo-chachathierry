---
title: Interpellation
date: 2019-02-24T14:53:52+01:00
draft: false
categories: ["Société"]
description: "Témoignage de Marc paru dans le journal LA SOURCE en Janvier 1986"
tags: ["Divers"] 
---

* * * 

Encore un 15 Avril ; 5 ans déjà, 5 longues années pénibles, mais je ne l'ai pas oubliée, encore moins que le jour où elle a disparu. Elle est partie, emmenant avec elle ce dégoût qu'elle avait de cette terre, qu'elle essayait d'oublier dans les bars, les hommes, l'alcool. Elle nous en faisait voir de toutes les couleurs; certains lui ont pardonné, ils la savaient plus malheureuse que nous. Quand j'étais enfant, je l'ai détestée pour les traitements qu'elle nous faisait supporter, mais après de longues années, je sais ce que c'est la souffrance, ça peut détruire une vie entière et changer un ange en démon.

Je comprends, maintenant, cette femme qui était ma mère. Elle souffrait depuis longtemps de toute ces années d'agonie dans la malchance ; elle s'est éteinte et beaucoup ont été soulagés. Mais moi, je suis vivant et je n'oublie pas, je peux porter un jugement sur les gens qui l'ont connue : ils sont abjects, écœurants, sournois, de sont des vautours, des charognards. Je déborde de colère parce que je n'ai pas pu intervenir : je n'étais qu'un enfant perdu qui ne comprenait rien à tout ce qui se passait autour de lui, choqué par toutes ces scènes qui auraient pu être dramatiques. Elle me donnait un calmant pour adulte, j'étais le turbulent de la maison : lorsqu'elle me tapait, je sentais que ce n'était pas elle, mais sa violence qui agissait en elle, folle de rage. Tout ceci venait de son passé : la mort de son père l'avait secouée ; l'alcool qui était son seul compagnon et qui lui faisait tout oublier ; les hommes chez qui elle cherchait l'affection mais qui s'en fichait pas mal : l'argent seul les intéressait ; les bars où elle se réfugiait : elle offrait des verres à tout le monde, elle y  passait ses journées, ne se rendait pas compte qu'on se fichait d'elle. C'est la dernière image qu'elle a emporté avec elle : un bar, des hommes, de l'alcool.

Elle ne voyait pas le derrière de ce maudit rideau, "ces gens pires que des tiques qui vous sucent le sang, cette vermine qui finit par vous vider".

Elle est morte, emportant avec elle tout son coffre de malheur, de souffrance, de haine, de violence.

Je n'avais que 15 ans ! ... mais j'en connaissais déjà long sur la vie ! ...

Lorsque j'étais à l'école, en récréation, j'étais toujours seul ou avec mes frères. Personne avec nous, nous formions un clan car on n'aimait personne. Je me souviens d'un gars de mon âge qui était à côté de moi, il se moquait en me disant: "Ma mère boit du lait, la tienne boit du rouge". alors je lui mettais un coup de pied. Il criait, le professeur demandait ce qui se passait, il levait la main en disant: "Marc me donne des coups de pieds". Le professeur me punissait. Le soir, vu que je n'aimais pas l'école, j'allais jouer aux cages à poules pour ne pas rentrer faire mes devoirs. J'avais souvent des punitions à faire signer, ma mère me foutait une raclée et me disait : "Tu ne les feras pas. Tu te débrouilleras avec l'instituteur ! " Il me les faisait faire pendant les récréations. il ne m'aimait pas vu que je ne payais jamais la cantine, que je ne donnais rien à la coopérative de la classe ( ma mère ne voulait pas ). Alors, je n'allais jamais aux voyages scolaires, ni voir les fêtes, les animaux. Rien quoi ! J'étais dans la classe avec d'autres qui n'y allaient pas, ils étaient malades ou autre chose. Par la fenêtre, je regardais les autres partir. J'avais un pincement dans la gorge, une boule ; je ne pouvais plus parler, à cause des larmes. Je ne disais rien. Je lisais un illustré. J'attendais 6 heures, car on me gardait aux heures d'études. 6 heures ! l'heure qui me faisait peur : j'allais renter chez moi et qu'allait-il se passer encore ? Une trempe ou traîner dans les cafés avec ma mère. Si elle me faisait suivre, elle devait m'acheter des bombons pour que je lui fiche la paix. Souvent je prenais deux baffes et je rentrais tout seul à la maison.

Ça a duré longtemps comme cela, jusqu'à l'age de 9 ans.

A cette époque, je suis parti car ma mère était en train de se faire déchoir de ses droits maternels. Je suis allé dans une pension. Je n'ai pu la revoir qu'après 3 ans d'attente. Là aussi,  j'ai eu d'autres pincements et des boules dans la gorge : je n'avais que 12 ans et déjà bagarreur.

Lorsque j'ai revu ma mère, j'étais heureux. Je suis sur que j'aurais été moins malheureux avec elle que là où j'étais : je n'aimais pas les groupes, les dortoirs, les ordres, les punitions. Je n'aimais pas qu'on me mente car on me disait : la première année tu verras ta mère aux prochaines vacances. Je voyais les autres partir, moi j'allais dans une colonie, pas chez moi. "Ils" m'ont menti pendant 3 ans. On me punissait quand je ne voulais pas jouer avec les autres. J'étais habitué aux coups, J'ai fini par riposter.

Enfin, après ces trois années d'attente, j'ai revu ma mère: l'alcool l'avait vieillie mais elle avait toujours le même regard et je comprenais que sa vie n'avait pas changée, qu'elle me battrait encore. Mais, j'étais avec elle. Je l'aimais quand même.

Les vacances ont fini. Il fallait repartir, je ne voulais pas. Une assistante sociale est venue me chercher. Je l'ai faite courir dans la cité. Je voulais rester avec ma mère. Un bonhomme m'a attrapé. Il m'a fait peur en me disant qu'il était gendarme, qu'il fallait que je reparte là-bas sinon on mettrait ma mère en prison. Je suis reparti. De retour à la pension, j'ai tout fait pour me faire renvoyer : des petits vols, etc ... A chaque fois on me disait : "privé de permission pour les prochaines vacances".

J'ai revu ma mère 2 ans après : pour sa mort. J'avais 15 ans.

J'ai très mal supporté. je me suis renfermé.

Lorsque je l'ai vue morte, à la morgue, je me suis dit: au moins maintenant tu ne souffres plus ! Je l'ai embrassée ainsi que toute ma famille. Le lendemain, nous nous sommes revus pour aller l'enterrer au cimetière de Villejuif où elle repose.

Je l'imagine encore dans son cercueil : elle avait les yeux tristes, la peau toute ridée et beaucoup de cheveux blancs. Je lui ai mis des roses.

De force, on m'a ramené au Centre. 3 mois après on m'a renvoyé. J'étais intenable, j'allais contre toutes les règles, je n'avais peur de rien n'y de personne. De retour à Paris, je suis allé dans un Centre d'où je m'évadais souvent pour me battre dehors. J'étais devenu violent, agressif, avec toute cette haine pour une société injuste et une justice incompréhensible qui me dégoûtaient ; puis j'ai pris une cuite, une autre, et une autre. Puis encore une au Centre... où j'ai eu quelques aventures malchanceuses.

Mon premier travail m'a permis de partir dans un foyer de jeunes travailleurs d'où j'ai été renvoyé pour avoir fait des menaces au Directeur. Je ne l'aimais pas beaucoup car chaque fois qu'il me voyait, il me disait : "Dis Marc, que veulent dire tes tatouages ?" Alors, je lui répondais ce que ma mère me disait souvent lorsque je lui posais des questions qui la gênaient : "Si on te demande, tu diras que tu n'en sais rien ... et maintenant va te faire foutre". La première fois que je lui ai dis cela, il était fou de rage. Je voulais qu'on me laisse tranquille. Mon passé ne le regardait pas. Je voulais être tranquille. Pour lui, j'étais un garçon qui travaillait, donc un brave gars. Mais, j'avais les cheveux longs, des tatouages et je fréquentais des gens peu recommandables : ils étaient les seuls avec lesquels je m'entendais. Les autres parlaient politique et de gens célèbres, moi je ne comprenais rien . Je leur disais : "C'est beau de savoir parler mais il faut regarder autour de soi". Ils parlaient des orphelins et disaient que les parents qui abandonnaient leurs enfants n'étaient pas des êtres mais des bêtes. Ils me demandaient mon avis. Tout cela finissait très mal car je leur disais que mes parents étaient des êtres que la souffrance et l'alcool avaient rendus très méchants, mais que ce n'étaient pas des gens à juger à part. L'un de ces gars m'a choqué, il m'a dit : "Ta mère devait être une putain et ton père un maquereau, toi, un pauvre type qui ne s'est aperçu de rien".

Alors la bagarre a commencé et je me suis vengé de tout ce qu'il pensait de mes parents.

D'autres aventures se sont ajoutés à celle là. elles m'ont fait très mal et rendu très violent car on avait remué mon passé déjà lourd de peines et de chagrins.

Je me suis dit "il n'y a que toi qui peux juger ton passé car tu l'as vaincu de force. Qui peut te reprocher ton sale caractère, ta violence, sur tout ? Même si les gens disaient "qu'ils" étaient écœurants, je garde en moi une image : ma mère en train de pleurer et me disant : "Tu sais Marc, la guerre et les peines de la vie changent un être ! "

Elle était consciente de son malheur mais elle était habituée à boire... Sur cette terre, il faut du temps pour réaliser qui on est par rapport à son passé. On devrait pouvoir convaincre les gens et leurs dire:

- "Regarde autour de toi, il y a plus malheureux, ne juge pas , ne condamne pas, ne rejette pas".
- J'ai souvent pleuré, j'ai souvent envié. Je n'avais jamais aimé, seulement respecté.
- Je l'ai appris très tard, après beaucoup de peines et larmes, ce mot "bonheur".
- Le bonheur de se sentir bien dans sa peau, de ne pas se sentir rejeté. Je suis heureux, car j'ai appris à me battre contre et pour la vérité.
- Peut-on me dire pourquoi il faut faire taire ces gens que l'ont dit : marginaux - délinquants - drogués ?
- Pourquoi ? parce que dans ce monde, la parole est aux gens respectables, qui travaillent, paient leurs impôts. Mais que fait-on pour un être comme moi, bourré de problèmes ? 
- Il vous faudra comprendre un jour ce jeune ivrogne, violent et agressif ! 
- On peut parler des avions, de la nature, de tous ces faux problèmes, mais il y a autre chose sur terre: la faim, les enfants malheureux. Pourquoi les mettres dans des Centres, ils sont comme dans une prison et finissent par y aller.
- Vous gens honnêtes et honorables, de quel droit jugez-vous ? 
- Pourquoi me dire que je suis un vaurien, pourquoi cette étiquette ? 
- Pouvez-vous me dire pourquoi j'ai tant souffert, pourquoi je ne pense qu'à ma venger ? 
- Je suis un être, avec mes angoisses, mes envies de suicides. Pourquoi, pourquoi cette pierre que je porte en moi ?
- Je suis né dans un milieu de violence où les coups étaient la règle ; élevé dans des centres où les coups étaient encore la règle. Pourquoi ? Comment voulez-vous que je ne sois pas violent ? Pourquoi tous les vols que j'ai commis ?
- Réfléchissez ! vous comprendrez pourquoi je ne suis jamais heureux là où je suis.
- Vous seuls pouvez répondre. Combien de délinquants, de voyous, de voleurs le sont devenus à cause de votre société irréprochable.
- Je n'ai pas de religion, aucune foi, aucune opinion vraiment importante (politique). Je me fous de tout cela.
- Pendant que vous perdez du temps à des questions secondaires, les marginalisés deviennent de plus en plus nombreux, la délinquance augmente ...
- Je ne peux vraiment pas vous comprendre !

## Contributing

- Témoignage de Marc (Le Journal de la Source N°32 Janvier 1986)

## License

### Fin

