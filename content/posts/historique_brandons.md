---
title: Historique Brandons
date: 2019-02-24T15:08:57+01:00
draft: false
categories: ["Société"]
description: "Comment et pourquoi à débuter le foyer des Brandons."
tags: ["Divers"] 
---

## Histoire vraie d'une création !

### Le Foyer Les Brandons

Il était une fois, une Maison d'enfants dite « à caractère social » : façon pudique d'expliquer sans faire trop mal à ceux qui s'y trouvent, qu'il s'agit d'un internat spécialisé avec classes plus ou moins adaptées à ces enfants de familles plutôt en grande difficulté.  Ces difficultés ont pour origine des raisons, particulières ou multiples : de santé, par exemple, mais surtout sociales, éducatives, économiques et culturelles. Ces enfants se trouvent donc pour ces raisons ou d'autres qui leur sont propres, en situation personnelle perturbée. A cette époque, la plupart des enfants peuvent se trouver placés dans l'Institution quand ils ont atteint 6 ans. Ils la quittent à 10 ou 11 ans. Le plus souvent pour être placés ailleurs.

#### 1969

Les enfants et leurs éducateurs sont logés dans un ancien château situé en plein espace boisé d'une trentaine d'hectares. L'édifice est en bord de Seine et à quelques kilomètres des communes voisines entre Champagne sur Seine et Samoreau. Magnifique isolement, sans aucun doute favorable à la paix et à la tranquillité de tous ceux que l'existence ou les déficiences et troubles du comportement de ces enfants dérangent ou inquiètent, mais pas du tout à leur socialisation ! C'est pourtant l'un des principaux objectifs de l'acte éducatif ! Est-il besoin de le rappeler à une Institution dite « à caractère social » ?

Fidèle à sa responsabilité et à cet objectif qui nécessite une durée suffisante et un travail éducatif cohérent, l'équipe éducative obtient des autorités administratives dont dépend la Maison d'enfants, le Président de la Fondation propriétaire et le Directeur de l'Action Sociale du Département, l'autorisation de garder les enfants jusqu'à 14 ans et d'ouvrir la maison de manière à travailler avec les institutions des communes voisines, en commençant par inscrire ces enfants dans les Ecoles publiques et en leur donnant la possibilité de participer aux organisations et animations culturelles du secteur : sports, loisirs et cultes.

![Chateau des Pressoirs du roy](normal-w960.webp)

[Source image](https://www.geneanet.org/cartes-postales/view/6342420#0)

#### 1976

En 1976, l'équipe se trouve confrontée à un problème nouveau. Parmi les jeunes qui ont atteint leurs 14 ans, la limite d'âge imposée, quelques uns sont inquiets et même angoissés. Sans famille capable de les accueillir, sans aucune relation privilégiée, ici ou là, ils dépendent de l'administration et ils ignorent tout de leur future destination ! Ils risquent d'être dispersés et placés de grés ou de force, dans une autre Institution, sans doute elle-aussi « à caractère social ! ».

L'équipe estime qu'un tel changement en pleine période initiatrice, est vraiment déconseillé ! Ces jeunes ont accompli une première étape de socialisation. Celle-ci  est insuffisante : les acquis  sont encore fragiles. Il semble donc indispensable que ces quelques adolescents :

* puissent encore bénéficier de la proximité de quelques uns des compagnons avec qui ils ont vécu au moins deux années d'apprentissage scolaire et social ;
* qu'ils puissent continuer de s'insérer dans la communauté d'habitants de Champagne sur Seine grâce aux écoles, aux divers groupes d'activités de la commune et aux familles qui les ont reçus de temps en temps ;
* qu'ils puissent encore être accompagnés par quelques uns des membres d'une équipe éducative adaptée à leur situation et à leurs nouveaux besoins de tous genres. Il n'y a pas de maturité sans un temps suffisant et beaucoup de patience.

Ces jeunes se confient à leurs éducateurs. Ils demandent à rester dans le cadre éducatif et familier qu'ils connaissent bien, auquel ils se sont habitués, et dans lequel ils ont commencé à développer leurs facultés et leurs capacités. Ils désirent aussi rester à proximité des personnes qu'ils connaissent depuis plusieurs années: évidemment leurs copains et de leurs copines, mais aussi des adultes ; familles, responsables de clubs, professeurs du collège de Champagne sur Seine.

L'équipe éducative estime alors qu'un petit Foyer d'une dizaine de places, situé en plein centre de la commune de Champagne sur Seine, lui permettrait de répondre non seulement au désir de ces garçons, mais aussi au sien. Ce Foyer deviendrait le lieu de vie des aînés de la Maison d'enfants et leur offrirait la présence dont ils ont encore besoin pour atteindre le niveau de stabilité et de cohérence indispensable à leur vie personnelle et sociale.

L'équipe a l'audace de croire à sa capacité de trouver l'argent nécessaire pour l'achat d'un lieu d'habitation. Elle sait pouvoir compter sur beaucoup d'amis et quelques associations pour obtenir des dons et des prêts de service. Et elle espère obtenir des autorités une augmentation de l'effectif des jeunes placés dans la Maison d'enfants : condition incontournable pour que se réalise l'opération et garanti l'équilibre du budget.

Le Projet est présenté aux deux autorités responsables : celles de la Fondation, propriétaire des lieux et celles de la Direction des Services Sociaux du Département. La réponse est immédiate : l'extension de l'effectif est refusé. Le projet de l'équipe paraît pratiquement insensé !

La directrice de la Maison d'enfants et la plupart des membres de l'équipe éducative se mobilisent et décident alors de prendre le risque de matérialiser le Projet de telle manière que les autorités découvrent son importance et sa valeur.

Une maison enrichie de deux petits bâtiments en dépendance et d'un jardin est à vendre dans la Commune de Champagne sur Seine. Maison  bien située, en pleine ville et près de la gare. Elle peut accueillir au moins 8 jeunes et deux éducateurs. A proximité de cette maison, sur le cadastre, les lieux portent le nom de « Brandons » Ce nom désigne de petites torches de pailles enflammées servant à éclairer la campagne et allumer des feux, mais c'était aussi le surnom de celui qui allumait les feux de joie..

Pourquoi ne pas choisir pour ce lieu éducatif un nom qui porte en lui tant de significations positives aussi bien  pour des éducateurs que pour des jeunes ? C'est bien de cela qu'il s'agit de faire pour eux : les réchauffer, les éclairer, leur permettre de se transformer et de devenir eux-mêmes « lumière et chaleur » d'une société en mal de sens !

Pour acheter cette maison et créer un Foyer, il est indispensable de commencer par fonder une Association à objectif éducatif. Ce sera fait le 10 Décembre 1976, l'Association « Les Brandons » (ALB) est en ordre de marche. Elle comprend la plupart des membres de l'équipe éducative, des amis de la région immédiate mais aussi des connaissances dispersées dans différents lieux de France. Ces amis sont, eux aussi, partisans de ce Projet.

Le psychopédagogue de l'équipe est élu Président et la directrice, Secrétaire. Le trésorier, est un père de famille de Champagne sur Seine. Les statuts sont déposés la veille de Noël 1976 à la Préfecture de Melun.

#### 1977

Le Journal Officiel du 22 janvier 1977 publie la déclaration des statuts de l'Association Les Brandons (ALB) et son objet.

L'ALB s'enrichit alors de nombreux associés : En Juin 1977, ils sont 177, et tout le monde s'est mobilisé pour trouver les 230.000 francs nécessaires à l'achat de la maison et au 70 000 demandés pour l'aménagement.: tout ceci sous forme de dons et de prêts à taux zéro. A l'étonnement général, y compris de l'équipe elle-même, ce sera vite fait ! La maison est achetée et payée au mois de mars et elle sera rapidement aménagée .

L'équipe décide d'ouvrir ce nouveau Foyer, au début de septembre. Elle garde l'espoir que telle ou telle direction de l'Aide Sociale, y compris celle du département de Seine et Marne dont dépendent plusieurs garçons de la Maison d'enfants, acceptera de payer le « prix de journée » individuel, indispensable pour l'existence du Foyer et la vie des jeunes. L'ALB ne peut vivre et réaliser son objectif en ne se basant que sur des dons.

Une demande de reconnaissance de ce lieu de vie est à nouveau adressée aux deux autorités de tutelle de la Maison d'enfants. Face à la détermination de l'équipe et à la qualité du travail entrepris, celles-ci acceptent finalement le Projet de l'équipe. Et louent la maison  à l'Association.

Le nouveau Foyer est ouvert et inauguré le 9 octobre 1977 en présence du Maire de Champagne sur Seine. Désormais les jeunes de 14 à 18 ans peuvent achever leur formation à Champagne sur Seine..


![Photo groupe](P1-w960.webp)


#### 1981

C'est en février 1981 que, suite à un conflit institutionnel ayant amené en 1980, la démission d'une partie importante du personnel éducatif, le nouveau directeur de la Maison d'enfants décide d'arrêter la prise en charge du Foyer Les Brandons et disperse les jeunes qui s'y trouvent.

Le Président de l'ALB, face à la situation de ces jeunes renvoyés brutalement, et aussi, en raison de la demande que lui adresse la Direction de l'Aide Sociale d'un département proche dont font partie trois des jeunes exclus, décide la réouverture immédiate du Foyer. Il organise avec des membres éducateurs de l'Association, la prise en charge de ce petit groupe jusqu'en Juillet 1981, date à laquelle il nomme au Foyer un jeune directeur, éducateur spécialisé.

D'abord accepté comme « Lieu de vie » par les Services Sociaux des départements limitrophes, le Foyer sera bientôt reconnu et agréé comme Foyer d'accueil de jeunes en difficulté par la Direction de l'Action Sociale de Seine et Marne.


#### 1990

Au Foyer, ils sont maintenant 16 adolescents, âgés de 14 à 17 ans. De nouveaux aménagements ont permis d'augmenter leur nombre. S'ajoutent deux appartements loués à l'extérieur du Foyer pour les plus âgés.

Les autorités administratives estiment que le temps est venu d'envisager une sérieuse réhabilitation : sécurité, hygiène, espace, organisation de vie. On ne peut la réaliser sur place ! Ce sera à Morêt sur Loing que l'Association ALB et l'équipe éducative du Foyer, trouveront la maison qui convient : c'est « l'hôtel du Prieuré ». Il faudra 10 ans pour accomplir toutes les opérations nécessaires et donc intégrer le nouveau Foyer.

#### 1996

Depuis un an, soutenus par l'équipe éducative, le directeur du Foyer et sa compagne, mettent au point un nouveau projet éducatif. Plusieurs jeunes du Foyer ont un besoin manifeste de « sortir », au moins quelques mois, non seulement de leur environnement familial, mais aussi de toutes les structures de vie habituelles que leur offre l'institution. Il s'agit d'organiser avec eux un voyage de type initiatique capable de leur permettre une véritable conversion de leur personnalité et de leurs capacités de tous ordres.

L'accord de la Direction de l'Aide Sociale est acquis. Six jeunes et deux éducateurs quittent le Foyer les Brandons le 1er Avril 1996. Ils passeront 15 mois à parcourir l'Afrique sub-saharienne. Ce Foyer Mobile sera finalement considéré comme « Ecole Mobile ». Depuis, l'expérience continue, avec une dizaine de jeunes chaque année. Mais elle se poursuit sous le nom et la responsabilité d'une autre structure : l'Association « Pourquoi-Pas ? »

Entre le 1er septembre 1977 et le 1er octobre 2000, ce sont 250 jeunes qui ont passés quelques années au Foyer les Brandons à Champagne sur Seine.

#### 2000

Le 1er octobre 2000, le Foyer Les Brandons déménage. Il s'installe à Moret sur Loing. L'ancien hôtel a été rénové et transformé. C'est un lieu de vie agréable et de qualité où les jeunes peuvent être heureux de vivre malgré les difficultés que supposent un placement et des séparations douloureuses. Sur 18 jeunes placés au Foyer, 13 habitent dans les bâtiments de la propriété. Chacun a sa chambre-bureau, avec douche individuelle. Des salles sont à leur disposition : salles de réunion, d'activités, de loisirs, de soutien scolaire. Les 5 autres, si possible les plus âgés (17-18ans) vivent de manière plus autonome : 3 sont logés dans un des anciens bâtiments de l'hôtel et deux dans des appartements loués en ville. Ils expérimentent l'autonomie.

Entre le 1er octobre 2000 et le 1er Février 2009, 147 jeunes ont habité le Foyer Les Brandons à Moret sur Loing.

#### A la source

A la source de ce travail, une équipe de personnes inspirées par un projet chrétien d'éducation des jeunes en difficulté, travail initié par un personnage étonnant, historique, et quasiment mythique pour ses contemporains du 18ème siècle, Don Bosco, prêtre des rues d'alors, à Turin, capitale du Piémont Sardaigne. C'était avant et pendant la réalisation de l'unité Italienne.

Et pour ce travail, depuis quarante ans, aujourd'hui comme hier, avec un personnel de service ajouté mais lié par l'espoir commun, se trouve engagée une solide équipe professionnelle. Une équipe jeune de cœur et d'esprit, ouverte et dynamique. Une équipe fidèle au projet initial de référence, mais en quête d'une heureuse diversité éducative, non confessionnelle et inventive d'actualité et de présence. Une équipe toujours soucieuse de porter ensemble, sur ces jeunes bousculés par la vie et malgré leurs difficultés de parcours, un regard d'authentique amitié, de solide confiance, de joie simple et de courageuse patience.

J.P. Jung

###### 15 02 2009

{{< youtube 045WClNj3UI >}}

## Contributing

- [Les Brandons](http://www.lesbrandons.fr/)
- [Maison d’enfants Samoreau - 77](https://www.cognacq-jay.fr/etablissements/maison-denfants-77)
- [Fondation Cognacq Jay Les Pressoirs Du Roy](https://copainsdavant.linternaute.com/e/fondation-cognacq-jay-les-pressoirs-du-roy-1222659)
- [Wikipedia | Fondation Cognacq-Jay](https://fr.wikipedia.org/wiki/Fondation_Cognacq-Jay)

## License

### Fin


